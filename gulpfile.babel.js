import gulp from 'gulp';
import loadPlugins from 'gulp-load-plugins';
import webpack from 'webpack';
import rimraf from 'rimraf';

const plugins = loadPlugins();

import eventWebpackConfig from './app/event/webpack.config.babel';
import tabWebpackConfig from './app/tab/webpack.config.babel';

gulp.task('tab-js', ['clean'], (cb) => {
    webpack(tabWebpackConfig, (err, stats) => {
        if (err) throw new plugins.util.PluginError('webpack', err);

        plugins.util.log('[webpack]', stats.toString());

        cb();
    });
});

gulp.task('event-js', ['clean'], (cb) => {
  webpack(eventWebpackConfig, (err, stats) => {
    if(err) throw new plugins.util.PluginError('webpack', err);

    plugins.util.log('[webpack]', stats.toString());

    cb();
  });
});

gulp.task('tab-html', ['clean'], () => {
    return gulp.src('app/tab/src/index.html')
        .pipe(plugins.rename('newtab.html'))
        .pipe(gulp.dest('./build'))
});

gulp.task('copy-manifest', ['clean'], () => {
  return gulp.src('app/manifest.json')
    .pipe(gulp.dest('./build'));
});

gulp.task('clean', (cb) => {
  rimraf('./build', cb);
});

gulp.task('images', ['clean'], () => {
    return gulp.src('app/tab/data/backgrounds/*')
        .pipe(gulp.dest('./build/data/backgrounds'));
});

gulp.task('icons', ['clean'], () => {
    gulp.src('app/tab/data/icons/*.png')
        .pipe(gulp.dest('./build/data/icons/'));
    gulp.src('app/tab/src/favicon.ico')
        .pipe(gulp.dest('./build'));
});

gulp.task('locales', ['clean'], () => {
    gulp.src('app/event/_locales/**/*.json')
        .pipe(gulp.dest('./build/_locales'));
});

gulp.task('build', ['copy-manifest', 'tab-js', 'event-js', 'tab-html', 'images', 'icons', 'locales']);

gulp.task('watch', ['default'], () => {
  gulp.watch('app/event/**/*', ['build']);
  gulp.watch('app/tab/**/*', ['build']);
});

gulp.task('default', ['build']);
