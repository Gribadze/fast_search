import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Tabs, Tab, History, Bookmarks, TopSites } from '../Model';

class BackgroundContoller extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { updateTabs, updateHistory, updateBookmarks, updateTopSites } = this.props;
        chrome.tabs.query({
            url: 'chrome://newtab/'
        }, tabs => {
            tabs.forEach(tab => {
                chrome.tabs.update(tab.id, {
                    url: 'chrome://newtab/'
                });
            })
        });
        chrome.browserAction.onClicked.addListener(() => {
            chrome.tabs.create({
                url: 'newtab.html'
            });
        });
        chrome.tabs.onUpdated.addListener((id, changeInfo, tab) => {
            if (changeInfo.status === "complete") {
                updateTabs(tab.windowId);
                updateHistory();
            }
        });
        chrome.tabs.onActivated.addListener(activeInfo => updateTabs(activeInfo.windowId));
        chrome.tabs.onRemoved.addListener(() => updateTabs());
        chrome.windows.onFocusChanged.addListener(updateTabs);
        updateTabs();
        updateHistory();
        updateBookmarks();
        updateTopSites();
    }

    render() {
        return <h1>Hello, world :)</h1>
    }
}

const mapStateToProps = state => ({
    tabs: state.tabs,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    ...Tabs,
    ...Tab,
    ...History,
    ...Bookmarks,
    ...TopSites
}, dispatch);

const mergeProps = (stateProps, dispatchProps) => {
    const { tabs } = stateProps;
    const { getTabs, updateTab, updateHistory, updateBookmarks, updateTopSites } = dispatchProps;

    return {
        tabs,
        updateTabs: (windowId) => {
            if (!windowId) {
                chrome.windows.getCurrent({}, window => {
                    getTabs(tabs => {
                        const tab = tabs.find(tab => tab.active && tab.windowId === window.id);
                        updateTab(tab);
                    })
                })
            } else {
                getTabs(tabs => {
                    const tab = tabs.find(tab => tab.active && tab.windowId === windowId);
                    updateTab(tab);
                })
            }
        },
        updateHistory,
        updateBookmarks,
        updateTopSites
    }
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(BackgroundContoller);