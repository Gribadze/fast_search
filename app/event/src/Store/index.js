import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';

import { reducer, aliases } from '../Model';

import { wrapStore, alias } from 'react-chrome-redux';

const store = createStore(
    reducer,
    applyMiddleware(
        alias(aliases),
        thunkMiddleware/*,
        logger*/
    )
);

wrapStore(store, {
    portName: 'FS'
});

export default store;