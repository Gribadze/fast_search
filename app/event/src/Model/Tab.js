import { createActions, handleActions, combineActions } from 'redux-actions';

const {
    updateTab
} = createActions(
    'UPDATE_TAB'
);

const initState = {};

export const reducer = handleActions({
    [updateTab]: (state, { payload }) => {
        return {
            ...state,
            ...payload
        }
    }
}, initState);

export const model = {
    updateTab
};