import { createActions, handleActions, combineActions } from 'redux-actions';

const {
    updateBackgroundSettings
} = createActions(
    'UPDATE_BACKGROUND_SETTINGS'
);

const initState = {
    bgCount: window.fs_bg_count || 1,
    shuffle: localStorage.fs_shuffle !== "false",
    favorites: localStorage.fs_favorites && localStorage.fs_favorites.split(',').map(i => (+i)) || [],
    favorites_only: !localStorage.fs_favorites_only === "false",
    search_engine: window.fs_search_engine || 'http://searchkska.xyz/ap/?n=40517&id=SY1000705&q='
};

export const reducer = handleActions({
    [updateBackgroundSettings]: (state, { payload: { param, paramName, value } }) => {
        localStorage[paramName] = value;
        return {
            ...state,
            [param]: value
        }
    }
}, initState);

export const model = {
    updateBackgroundSettings
};