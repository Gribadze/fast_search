import { createActions, handleActions, combineActions } from 'redux-actions';

const {
    sendRequest,
    sendProcess,
    sendSuccess,
    sendError
} = createActions(
    'SEND_REQUEST',
    'SEND_PROCESS',
    'SEND_SUCCESS',
    'SEND_ERROR'
);

const initState = {};

export const reducer = handleActions({
    [sendProcess]: (state, { payload: { method, data } }) => {
        return {
            ...state,
            [method]: {
                ...[method],
                data
            }
        }
    },
    [combineActions(sendSuccess, sendError)]: (state, { payload: { method, response } }) => {
        return {
            ...state,
            [method]: {
                ...[method],
                response
            }
        }
    }
}, initState);

const _sendProcess = (method, data) => dispatch => {
    dispatch(sendProcess({ method, data }));
    return new Promise((resolve, reject) => {
        const url = `http://185.22.62.115:9000/api/?method=${method}&extId=${chrome.runtime.id}`+
            `&extVersion=${chrome.runtime.getManifest().version}&query=${encodeURIComponent(data)}`;
        fetch(url).then(response => response.text()).then(body => {
            resolve(body);
        }).catch(error => {
            reject(error);
        });
    }).then(
        body => dispatch(sendSuccess({ method, response: body}))
    ).catch(
        error => dispatch(sendError({ method, error }))
    )
};

export const aliases = {
    [sendRequest]: (action) => {
        const { payload: { method, data }} = action;
        return _sendProcess( method, data );
    }
};

export const model = {
    sendRequest
};

