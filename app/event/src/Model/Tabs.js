import { createActions, handleActions, combineActions } from 'redux-actions';

const {
    registerTab,
    deleteTab,
    getTabs,
    updateTabs,
} = createActions(
    'REGISTER_TAB',
    'DELETE_TAB',
    'GET_TABS',
    'UPDATE_TABS'
);

const initState = [];

export const reducer = handleActions({
    [registerTab]: (state, { payload: tab }) => {
        return [
            ...state,
            tab
        ];
    },
    [deleteTab]: (state, { payload: id }) => {
        return state.filter(tab => tab.id !== id)
    },
    [getTabs]: (state, { payload }) => {
        return payload;
    },
    [updateTabs]: (state, { payload: { id, changeInfo } }) => {
        return state.map(tab => {
            if (tab.id === id) {
                return {
                    ...tab,
                    ...changeInfo
                };
            }
            return tab;
        })
    }
}, initState);

export const model = {
    registerTab,
    deleteTab,
    getTabs: (callback) => dispatch => {
        chrome.tabs.query({}, tabs => {
            dispatch(getTabs(tabs));
            if (callback) callback(tabs);
        })
    },
    updateTabs
};