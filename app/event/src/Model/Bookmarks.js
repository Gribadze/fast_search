import { createActions, handleActions, combineAction } from 'redux-actions';

const {
    updateBookmarks
} = createActions(
    'UPDATE_BOOKMARKS'
);

const initState = [];

export const reducer = handleActions({
    [updateBookmarks]: (state, { payload }) => {
        return payload;
    }
}, initState);

const foo = (arr, node) => {
    if (!node.children) return [ ...arr, node ];
    for (let i = 0; i < node.children.length; i++ ) {
        arr = foo(arr, node.children[i]);
    }
    return arr;
};

export const model = {
    updateBookmarks: () => dispatch => {
        let bookmarks = [];

        chrome.bookmarks.getTree(tree => {
            for (let i = 0; i < tree.length; i++) {
                bookmarks = foo(bookmarks, tree[i]);
            }
            dispatch(updateBookmarks(bookmarks));
        });
    }
};