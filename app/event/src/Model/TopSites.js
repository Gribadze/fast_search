import { createActions, handleActions, combineAction } from 'redux-actions';

const {
    updateTopSites
} = createActions(
    'UPDATE_TOP_SITES'
);

const initState = [];

export const reducer = handleActions({
    [updateTopSites]: (state, { payload }) => {
        return payload;
    }
}, initState);

export const model = {
    updateTopSites: () => dispatch => {
        chrome.topSites.get(urls => {
            dispatch(updateTopSites(urls));
        });
    }
};