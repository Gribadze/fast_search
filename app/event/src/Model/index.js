import { combineReducers } from 'redux';

import { reducer as backendReducer, model as Backend, aliases as backendAliases } from './Backend';
import { reducer as settingsReducer, model as Settings } from './Settings';
import { reducer as tabsReducer, model as Tabs } from './Tabs';
import { reducer as tabReducer, model as Tab } from './Tab';
import { reducer as historyReducer, model as History, aliases as historyAliases } from './History';
import { reducer as bookmarksReducer, model as Bookmarks } from './Bookmarks';
import { reducer as topSitesReducer, model as TopSites } from './TopSites';

export const reducer = combineReducers({
    backend: backendReducer,
    settings: settingsReducer,
    tabs: tabsReducer,
    tab: tabReducer,
    history: historyReducer,
    bookmarks: bookmarksReducer,
    topSites: topSitesReducer
});

export const aliases = {
    ...backendAliases,
    ...historyAliases
};

export {
    Backend,
    Settings,
    Tabs,
    Tab,
    History,
    Bookmarks,
    TopSites
};