import { createActions, handleActions, combineAction } from 'redux-actions';

const {
    fetchHistory,
    updateHistory
} = createActions(
    'FETCH_HISTORY',
    'UPDATE_HISTORY'
);

const initState = [];

export const reducer = handleActions({
    [fetchHistory]: (state, { payload }) => {
        return payload;
    }
}, initState);

const _updateHistory = () => dispatch => {
    return new Promise((resolve, reject) => {
        chrome.history.search({ text: '' }, historyItems => {
            resolve(historyItems);
        });
    }).then(items => dispatch(fetchHistory(items)))
};

export const aliases = {
    [updateHistory]: (action) => _updateHistory()
};

export const model = {
    updateHistory
};