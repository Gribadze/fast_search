import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { Background } from './Controller';
import store from './Store';

ReactDOM.render(
    <Provider store={store}>
        <Background/>
    </Provider>,
    document.getElementById('root')
);