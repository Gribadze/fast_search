import path from 'path';
import webpack from 'webpack';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';

let config = {
    entry: {
        background: './app/event/src/index.js',
        event: './app/event/src/event.js',
        defaults: './app/event/src/defaults.js'
    },

    output: {
        path: path.resolve(__dirname, '../../build'),
        filename: '[name].js',
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(jpe?g|png)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: (file) => {
                            if (process.env.NODE_ENV === 'development') {
                                return '[path][name].[ext]';
                            }

                            return './data/[name].[ext]';
                        }
                    }
                }
            },
        ]
    },

    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || "development")
        })
    ],

    devtool: process.env.NODE_ENV !== 'production' && 'inline-cheap-module-source-map',

    resolve: {
        extensions: ['.js', '.json'],
        modules: ['node_modules'],
        alias: {
            tab: path.resolve(__dirname, '../tab/src')
        }
    },
};

if (process.env.NODE_ENV === "production")
    config.plugins.push(new UglifyJsPlugin({
        uglifyOptions: {
            compress: {
                warnings: false
            },
            output: {
                comments: false
            }
        },
        sourceMap: true
    }));

export default config;