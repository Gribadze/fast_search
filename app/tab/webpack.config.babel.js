import path from 'path';
import webpack from 'webpack';
// import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';

let config = {
    entry: {
        tab: './app/tab/src/index.js',
    },

    output: {
        path: path.resolve(__dirname, '../../build'),
        filename: 'tab.js',
        publicPath: '/'
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.html$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        minimize: false
                    }
                }
            },
            {
                test: /\.(jpe?g|png)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: (file) => {
                            if (process.env.NODE_ENV === 'development') {
                                return '[path][name].[ext]';
                            }

                            return './data/[name].[ext]';
                        }
                    }
                }
            },
            {
                test: /\.css$/,
                use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true
                    }
                }]
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    }, {
                        loader: 'less-loader',
                        options: {
                            sourceMap: true,
                            resolve: 'url'
                        }
                    }]
                })
            }
        ]
    },

    plugins: [
        // new HtmlWebpackPlugin({
        //     title: 'Fast search',
        //     template: path.resolve(__dirname, 'src/newtab.html'),
        //     filename: path.resolve(__dirname, '../../build/newtab.html')
        // }),
        new webpack.NoEmitOnErrorsPlugin(),
        new ExtractTextPlugin('main.css'),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || "development")
        })
    ],

    devtool: process.env.NODE_ENV !== 'production' && 'inline-cheap-module-source-map',

    devServer: {
        contentBase: path.join(__dirname, 'build'),
        compress: false,
        port: 8080
    },

    resolve: {
        extensions: ['.js', '.jsx', '.less', '.json'],
        modules: ['node_modules']
    },
};

if (process.env.NODE_ENV === "production")
    config.plugins.push(new UglifyJsPlugin({
        uglifyOptions: {
            compress: {
                warnings: false
            },
            output: {
                comments: false
            }
        },
        sourceMap: true
    }));

export default config;