import { connect } from 'react-redux';

import Views from '../Views';
import Models from '../Models';

const mapStateToProps = state => ({
    tabId : state.tabs.active,
    history : state.tabs.history,
    opened : state.tabs[ state.tabs.active ] && state.tabs[ state.tabs.active ].history.opened
});

const mapDispatchToProps = dispatch => ({
    toggleOpened: (tabId) => {
        dispatch({
            type: Models.AppModel.CLOSE_ALL_POPUPS,
            payload: {
                tabId
            }
        });
        dispatch({
            type: Models.HistoryModel.TOGGLE,
            payload : {
                tabId
            }
        })
    }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { tabId, ...otherProps } = stateProps;
    const { toggleOpened } = dispatchProps;
    return {
        ...ownProps,
        ...otherProps,
        toggleOpened: () => toggleOpened(tabId)
    }
};

const TopSitesController = connect(mapStateToProps, mapDispatchToProps, mergeProps)(Views.HistoryView);

export default TopSitesController;