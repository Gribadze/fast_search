import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Tab } from '../Views';
import { Backend, Settings, History, Bookmarks } from '../Models';

const handleUninstall = () => {
    chrome.management.uninstallSelf({ showConfirmDialog: true });
};

class TabController extends React.Component {
    constructor(props) {
        super(props);

        this.handleDeleteHistory = this.handleDeleteHistory.bind(this);
    }

    componentDidMount() {
        const { getNextBg } = this.props;
        const bgId = getNextBg();
        document.body.style.backgroundImage = `url('/data/backgrounds/bg-${bgId}.jpg')`;
    }

    handleDeleteHistory(item) {
        const { updateHistory } = this.props;
        chrome.history.deleteUrl({
            url: item.url
        }, () => updateHistory())
    }

    render() {
        const {
            settings,
            history,
            bookmarks,
            topSites,
            sendRequest,
            updateSetting
        } = this.props;
        return (
            <Tab
                history={history}
                bookmarks={bookmarks}
                topSites={topSites}
                settings={settings}
                updateSetting={updateSetting}
                onSearchSubmit={(query) => {
                    sendRequest({ method: 'search_log', data: '' });
                }}
                onDeleteHistory={this.handleDeleteHistory}
                onUninstall={handleUninstall}
            />
        );
    }
}

const mapStateToProps = state => ({
    settings: state.settings,
    history: state.history,
    bookmarks: state.bookmarks,
    topSites: state.topSites,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    ...Backend,
    ...Settings,
    ...History
}, dispatch);

const mergeProps = (stateProps, dispatchProps) => {
    const {
        history,
        bookmarks,
        topSites,
        settings
    } = stateProps;
    const {
        bgCount,
        shuffle,
        favorites_only,
        favorites,
    } = settings;
    const { sendRequest, updateBackgroundSettings, updateHistory } = dispatchProps;
    return {
        settings,
        history,
        bookmarks,
        topSites,
        getNextBg: () => {
            const lastBgId = localStorage.fs_lastBgId !== undefined ?
                    (!isNaN(+localStorage.fs_lastBgId) ? +localStorage.fs_lastBgId : 1)
                    : 1;
            let bgId = lastBgId % bgCount + 1;
            if (shuffle) {
                if (favorites_only) {
                    bgId = favorites[Math.floor(Math.random() * favorites.length)];
                } else {
                    const bgList = [
                        ...Array.apply(null, {length: bgCount}).map((dummy, index) => index + 1),
                        ...favorites
                    ];
                    bgId = bgList[Math.floor(Math.random() * bgList.length)];
                }
            } else if (favorites_only) {
                const lastFavorite = favorites.indexOf(lastBgId);
                if (lastFavorite !== -1) {
                    bgId = favorites[(lastFavorite + 1) % favorites.length];
                } else {
                    bgId = favorites[0];
                }
            }
            localStorage.fs_lastBgId = bgId;
            return bgId;
        },
        sendRequest,
        updateSetting: (param, paramName, value) => {
            updateBackgroundSettings({ param, paramName, value });
        },
        updateHistory
    };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TabController)