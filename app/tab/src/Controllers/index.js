import AppController from './AppController';
import SearchController from './SearchController';
import OptionsController from './OptionsController';
import TopSitesController from './TopSitesController';
import HistoryController from './HistoryController';
import Tab from './Tab';

export const Controllers = {
    AppController,
    SearchController,
    OptionsController,
    TopSitesController,
    HistoryController
};

export {
    Tab
};

export default Controllers;