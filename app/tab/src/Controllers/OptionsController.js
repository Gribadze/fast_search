import { connect } from 'react-redux';

import Views from '../Views';
import Models from '../Models';

const mapStateToProps = state => ({
    tabId: state.tabs.active,
    tabState: state.tabs[state.tabs.active],
    bgCount: state.settings.bgCount,
    favorites: state.settings.favorites,
    shuffle: state.settings.shuffle,
    favorites_only: state.settings.favorites_only
});

const mapDispatchToProps = dispatch => ({
    toggleOpened: (tabId) => {
        dispatch({
            type: Models.AppModel.CLOSE_ALL_POPUPS,
            payload: {
                tabId
            }
        });
        dispatch({
            type: Models.OptionsModel.TOGGLE,
            payload: {
                tabId
            }
        })
    },
    changeBackground: (tabId, bgId) => {
        document.body.style.backgroundImage = `url('/data/backgrounds/bg-${bgId}.jpg')`;
        dispatch({
            type: Models.OptionsModel.CHANGE_BG,
            payload: {
                tabId,
                bgId
            }
        })
    },
    toggleFavorite: (tabId, bgId) => {
        dispatch({
            type: Models.AppModel.TOGGLE_FAVORITE,
            payload: {
                tabId,
                bgId
            }
        })
    },
    toggleShuffle: (tabId) => {
        dispatch({
            type: Models.AppModel.TOGGLE_SHUFFLE,
            payload: {
                tabId
            }
        })
    },
    toggleFavoritesOnly: (tabId) => {
        dispatch({
            type: Models.AppModel.TOGGLE_FAVORITES_ONLY,
            payload: {
                tabId
            }
        })
    }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { tabId, tabState, ...otherProps } = stateProps;
    const {
        toggleOpened,
        changeBackground,
        toggleFavorite,
        toggleShuffle,
        toggleFavoritesOnly
    } = dispatchProps;
    return {
        ...ownProps,
        ...otherProps,
        opened: tabState && tabState.options && tabState.options.opened || false,
        toggleOpened: () => toggleOpened(tabId),
        changeBackground: (bgId) => changeBackground(tabId, bgId),
        toggleFavorite: (bgId) => toggleFavorite(tabId, bgId),
        toggleShuffle: () => toggleShuffle(tabId),
        toggleFavoritesOnly: () => toggleFavoritesOnly(tabId)
    }
};

const OptionsController = connect(mapStateToProps, mapDispatchToProps, mergeProps)(Views.OptionsView);

export default OptionsController;