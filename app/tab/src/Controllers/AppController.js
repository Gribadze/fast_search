import { connect } from 'react-redux';

import Views from '../Views';
import Models from '../Models';

const mapStateToProps = state => ({
    tabId : state.tabs.active,
    bgId : state.tabs[ state.tabs.active ] && state.tabs[ state.tabs.active ].options.currentBg,
    ...state.settings
});

const mapDispatchToProps = dispatch => ({
    mount: (tabId, lastBgId, bgCount, favorites, shuffle, favorites_only) => {
        let bgId = lastBgId % bgCount + 1;
        if (shuffle) {
            if (favorites_only) {
                bgId = favorites[Math.floor(Math.random() * favorites.length)];
            } else {
                const bgList = [
                    ...Array.apply(null, {length: bgCount}).map((dummy, index) => index + 1),
                    ...favorites
                ];
                bgId = bgList[Math.floor(Math.random() * bgList.length)];
            }
        } else if (favorites_only) {
            const lastFavorite = favorites.indexOf(lastBgId);
            if (lastFavorite !== -1) {
                bgId = favorites[(lastFavorite + 1) % favorites.length];
            } else {
                bgId = favorites[0];
            }
        }
        document.body.style.backgroundImage = `url('/data/backgrounds/bg-${bgId}.jpg')`;
        // document.body.style.backgroundColor = '#cccccc';
        dispatch({
            type: Models.OptionsModel.CHANGE_BG,
            payload: {
                tabId,
                bgId
            }
        });
    },
    closeAllPopups: (tabId) => {
        dispatch({
            type: Models.AppModel.CLOSE_ALL_POPUPS,
            payload: {
                tabId
            }
        })
    }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { tabId, bgId, bgCount, favorites, shuffle, favorites_only } = stateProps;
    const { mount, closeAllPopups } = dispatchProps;
    return {
        ...ownProps,
        mount: () => mount(tabId, bgId, bgCount, favorites, shuffle, favorites_only),
        closeAllPopups: () => closeAllPopups(tabId)
    }
};

const AppController = connect(mapStateToProps, mapDispatchToProps, mergeProps)(Views.AppView);

export default AppController;
