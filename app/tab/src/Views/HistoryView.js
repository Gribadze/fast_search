import React from 'react';

import { cutString } from '../common';

const HistoryView = ({ opened, history, toggleOpened }) => {
    return (
        <div id="history" className="menu-collapse">
            <a onClick={ e => {
                e.stopPropagation();
                toggleOpened()
            }}>History</a>
            {
                opened && <ul className="list-simple">
                    {
                        history.map((data, index) => (
                            <li key={ index }>
                                <a href={ data.url }>
                                    {
                                        cutString(data.title || data.url, 40)
                                    }
                                </a>
                            </li>
                        ))
                    }
                </ul>
            }
        </div>
    );
};

export default HistoryView;