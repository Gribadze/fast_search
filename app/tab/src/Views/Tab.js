import React from 'react';

import { Search, Menu, Options } from '../Components';
import { cutString } from '../common';

const Tab = ({
                 history,
                 bookmarks,
                 topSites,
                 settings,
                 updateSetting,
                 onSearchSubmit,
                 onDeleteHistory,
                 onUninstall
             }) => (
    <div id="app">
        <Options settings={settings} updateSetting={updateSetting}/>
        <Search
            search_engine={settings.search_engine}
            suggestSources={{
                history,
                bookmarks
            }}
            onSubmit={onSearchSubmit}
        />
        <footer>
            <Menu id={'top-sites'} name={'Popular sites'} >
                {
                    topSites.slice(0, 10).map((item, index) => (
                        <li key={ index }>
                                    <span>
                                        <img src={`https://www.google.com/s2/favicons?domain=${(/:\/\/(.[^/]+)/).exec(item.url)[1]}`} />
                                    </span>
                            <a href={ item.url }>
                                {
                                    cutString(item.title || item.url, 40)
                                }
                            </a>
                        </li>
                    ))
                }
            </Menu>
            <Menu id={'history'} name={'History'}>
                {
                    history.slice(0, 10).map((item, index) => (
                        <li key={ index }>
                                    <span>
                                        <img src={`https://www.google.com/s2/favicons?domain=${(/:\/\/(.[^/]+)/).exec(item.url)[1]}`} />
                                    </span>
                            <a href={ item.url }>
                                {
                                    cutString(item.title || item.url, 40)
                                }
                            </a>
                            <button className={'btn-corner cross'} onClick={() => onDeleteHistory(item)}>
                                &times;
                            </button>
                        </li>
                    ))
                }
            </Menu>
            <Menu id={'support'} className={'right'} name={'Support'}>
                <li><a onClick={onUninstall}>Delete extension</a></li>
            </Menu>
        </footer>
    </div>
);

export default Tab;