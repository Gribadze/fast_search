import React from 'react';

import { cutString } from '../common';

const TopSitesView = ({ opened, topSites, toggleOpened }) => {
    return (
        <div id="top-sites" className="menu-collapse">
            <a onClick={ e => {
                e.stopPropagation();
                toggleOpened();
            }}>Popular sites</a>
            {
                opened && (
                    <ul className="list-simple">
                        {
                            topSites.map((data, index) => (
                                <li key={index}>
                                    <a href={data.url}>
                                        {
                                            cutString(data.title, 40)
                                        }
                                    </a>
                                </li>
                            ))
                        }
                    </ul>
                )
            }
        </div>
    );
};

export default TopSitesView;