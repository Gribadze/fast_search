import React from 'react';

const SearchView = ({ value, change, submit }) => {
    return (
        <div id="search">
            <form onSubmit={event => {
                event.preventDefault();
                submit(value);
            }}>
                <div className="input-group">
                    <input type="search" name="q" placeholder="Fast search..." value={value}
                           onChange={event => change(event.target.value)} tabIndex="1" autoFocus={true}
                    />
                    <button type="submit" style={{
                        zIndex: '1'
                    }}>
                        &#x1f50d;
                    </button>
                </div>
            </form>
        </div>
    );
};

export default SearchView;