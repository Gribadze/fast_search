import AppView from './AppView';
import SearchView from './SearchView';
import OptionsView from './OptionsView';
import TopSitesView from './TopSitesView';
import HistoryView from './HistoryView';

import Tab from './Tab';

const Views = {
    AppView,
    SearchView,
    OptionsView,
    TopSitesView,
    HistoryView
};

export {
    Tab
}

export default Views;