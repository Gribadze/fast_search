import React from 'react';

const OptionsView = ({
                         // props
                         opened, bgCount, favorites, shuffle, favorites_only,
                         // methods
                         toggleOpened, changeBackground, toggleFavorite, toggleShuffle, toggleFavoritesOnly
                     }) => {
    return (
        <div id="options">
            <a onClick={ e => {
                e.stopPropagation();
                toggleOpened();
            }}>Options</a>
            {
                opened && (
                    <div id="options-popup" onClick={event => event.stopPropagation()} style={{
                        zIndex: '999'
                    }}>
                        <ul className="list-simple list-inline">
                            {
                                Array.apply(null, { length : bgCount }).map((dummy, index) => {
                                    const src = '/data/backgrounds/bg-' + (index + 1) + '.jpg';
                                    return (
                                        <li key={ index }>
                                            <div className={'circle'}>
                                                <img className="img-responsive-h bg-preview" src={ src } onClick={ () => {
                                                    changeBackground(index + 1)
                                                } }/>
                                            </div>
                                            <button className="btn-corner star" onClick={ () => toggleFavorite(index + 1) }>
                                                {
                                                    favorites.indexOf(index + 1) === -1 &&
                                                    <span>&#x2606;</span> ||
                                                    <span>&#x2605;</span>
                                                }
                                            </button>
                                        </li>
                                    );
                                })
                            }
                        </ul>
                        <div>
                            <div className="inline checkbox-group">
                                <input type="checkbox" id="shuffle"
                                       value={shuffle} checked={shuffle && "checked"}
                                       onChange={toggleShuffle}
                                />
                                <label htmlFor="shuffle">Shuffle</label>
                            </div>
                            <div className="inline checkbox-group">
                                <input type="checkbox" id="favorites_only"
                                       value={favorites_only} checked={favorites_only && "checked"}
                                       onChange={toggleFavoritesOnly}
                                />
                                <label htmlFor="favorites_only" onClick={() => {
                                    let warnBox = document.getElementById('favorites-warning');
                                    if (warnBox) {
                                        document.getElementById('favorites-warning').style.display = 'block';
                                    }
                                }}>Favorites only</label>
                            </div>
                            {
                                favorites.length === 0 && (
                                    <div id="favorites-warning" style={{color: 'red'}}>No favorites selected</div>
                                )
                            }
                        </div>
                    </div>
                )
            }
        </div>
    );
};

export default OptionsView;