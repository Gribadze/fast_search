import React from 'react';

import Controllers from '../Controllers';

class AppView extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.mount();
    }

    render() {
        return (
            <div id="app" onClick={this.props.closeAllPopups}>
                <Controllers.OptionsController/>
                <Controllers.SearchController/>
                <footer>
                    <Controllers.TopSitesController/>
                    <Controllers.HistoryController/>
                </footer>
            </div>
        );
    }
}

export default AppView;