const SearchModel = {
    CHANGE : 'SEARCH_CHANGE',
    SUBMIT : 'SEARCH_SUBMIT'
};

export default SearchModel;
