const OptionsModel = {
    TOGGLE : 'TOGGLE_OPTIONS',
    CHANGE_BG : 'CHANGE_BG'
};

export default OptionsModel;