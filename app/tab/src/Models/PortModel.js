import { createActions, handleActions, combineActions } from 'redux-action';

const {
    sendMessage,
    sendSuccess,
    sendError
} = createActions(
    'SEND_MESSAGE',
    'SEND_SUCCESS',
    'SEND_ERROR'
);

const initState = {
    fetching: false,
};

export const reducer = handleActions({
    [sendMessage]: (state, { payload: { name, data } }) => {
        return {
            ...state,
            [name]: {
                ...data
            }
        }
    },
    [combineActions(sendSuccess, sendError)]: (state, { payload: { name, data }}) => {
        return {
            ...state,
            [name]: {
                ...state[name],
                ...data
            }
        }
    }
}, initState);

export const model = {
    sendMessage: (appId, message) => dispatch => {

    }
}