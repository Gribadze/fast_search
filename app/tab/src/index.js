import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Store } from 'react-chrome-redux';

import { Tab } from './Controllers';

import './less/main.less';

const proxyStore = new Store({
    portName: 'FS'
});

const unsubscribe = proxyStore.subscribe(() => {
    unsubscribe();
    render(
        <Provider store={proxyStore}>
            <Tab/>
        </Provider>,
        document.getElementById('main')
    )
});