import React from 'react';

import { cutString, alpha } from '../common';

const showCount = 20;

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            query: '',
            suggests: [],
            selected: -1
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);

        this.oldQuery = '';
    }

    handleChange(event, value) {
        const { suggestSources } = this.props;
        const query = this.oldQuery = event && event.target.value || value;
        if (query !== '') {
            fetch(`http://www.google.com/complete/search?client=firefox&ie=utf-8&oe=utf-8&hl=en-US&gl=&q=${query}`)
                .then(response => response.json())
                .then(body => {
                    let suggests = body[1].map(item => ({
                        source: 'google',
                        value: item
                    }));
                    for (let source in suggestSources) {
                        if (suggestSources.hasOwnProperty(source)) {
                            suggests = [
                                ...suggestSources[source]
                                    .filter(({ title, url }) => title.match(query) || url.match(query))
                                    .slice(0, 3)
                                    .map(({ title, url }) => ({
                                        source,
                                        value: cutString(`${title} - ${url}`, 50),
                                        url
                                    })),
                                ...suggests
                            ]
                        }
                    }
                    this.setState({
                        ...this.state,
                        suggests,
                        query
                    })
                })
                .catch(error => console.log(error));
        } else {
            this.setState({
                ...this.state,
                suggests: [],
                selected: -1,
                query
            })
        }
    }

    handleSubmit(event, suggest) {
        const { search_engine, onSubmit } = this.props;
        let { query } = this.state;
        if (suggest) query = suggest.value;
        if (event) event.preventDefault();
        if (query.length === 0) return;
        if (onSubmit && (!suggest || suggest.source === 'google')) {
            onSubmit(query);
            if (query.match(/\.[^\.]+$/) && alpha.find(a => query.match(new RegExp(`^\\S+\\.${a}$`, 'i')))) {
                window.top.location.href = `http://${query}`;
                // console.log(`http://${query}`);
            } else {
                window.top.location.href = `${search_engine}${encodeURIComponent(query)}`;
                // console.log(`${search_engine}${encodeURIComponent(query)}`);
            }
        } else {
            window.top.location.href = suggest.url;
            // console.log(suggest.url);
        }
    }

    handleKeyDown(event) {
        const { suggests, selected } = this.state;
        switch (event.keyCode) {
            case 40:
                event.preventDefault();
                if (selected < showCount - 1) {
                    document.getElementById('query-input').value = suggests[selected + 1].url || suggests[selected + 1].value;
                    this.setState({
                        ...this.state,
                        selected : selected + 1,
                        query: suggests[selected + 1].value
                    });
                }
                break;
            case 38:
                event.preventDefault();
                if (selected >= 0) {
                    if (selected > 0) document.getElementById('query-input').value = suggests[selected - 1].url || suggests[selected - 1].value;
                    this.setState({
                        ...this.state,
                        selected: selected - 1,
                        query: selected > 0 && suggests[selected - 1].value || this.oldQuery
                    });
                }
                break;
            case 13:
                if (selected >= 0) {
                    event.preventDefault();
                    this.handleSubmit(null, suggests[selected]);
                }
                break;
            default:
                document.getElementById('query-input').focus();
                break;
        }
    }

    render() {
        const { query, suggests, selected } = this.state;
        return (
            <div id="search" onKeyDown={ this.handleKeyDown }>
                <form onSubmit={ this.handleSubmit }>
                    <div className="input-group">
                        <input id="query-input" type="search" name="q" placeholder="Fast search..."
                               defaultValue={ query } className={query !== '' && 'active'}
                               onChange={ this.handleChange } tabIndex="1" autoFocus={ true }
                        />
                        <button type="submit" style={ {
                            zIndex : '1'
                        } }>
                            &#x1f50d;
                        </button>
                    </div>
                </form>
                { suggests.length > 0 && <ul className={ 'list-simple list-suggests' }>
                    {
                        suggests.slice(0, showCount).map((s, i) => (
                            <li key={ i } className={ selected === i && 'active' || '' }
                               onClick={() => this.handleSubmit(null, s)} data-src={s.source}
                            >
                                {s.value}
                                {
                                    s.source === 'google' && (
                                        <span onClick={event => {
                                            event.stopPropagation();
                                            document.getElementById('query-input').value = s.value;
                                            this.handleChange(null, s.value)
                                        }}>
                                            🡬
                                        </span>
                                    )
                                }
                            </li>
                        ))
                    }
                </ul> }
            </div>
        );
    }
}

export default Search;