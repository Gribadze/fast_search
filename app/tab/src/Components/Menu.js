import React from 'react';

import { hasParent } from '../common';

class Menu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            opened: false
        };

        this.handleToggleOpened = this.handleToggleOpened.bind(this);
    }

    handleToggleOpened(event) {
        const { opened } = this.state;
        const { id } = this.props;
        event.stopPropagation();

        if (!opened) {
            document.addEventListener('click', this.handleToggleOpened)
        } else {
            if (hasParent(event.target, document.getElementById(id))) return;
            document.removeEventListener('click', this.handleToggleOpened)
        }

        this.setState({
            ...this.state,
            opened: !opened
        });
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleToggleOpened)
    }

    render() {
        const { id, className, name, children } = this.props;
        const { opened } = this.state;
        return (
            <div id={id} className={`menu-collapse ${className || ''}`}>
                <a onClick={ this.handleToggleOpened }>{ name }</a>
                {
                    opened && <ul className="list-simple">
                        { children }
                    </ul>
                }
            </div>
        );
    }
}

export default Menu;