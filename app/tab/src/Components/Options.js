import React from 'react';
import { hasParent } from '../common';

const handleChangeBackground = (bgId) => {
    localStorage.fs_lastBgId = bgId;
    document.body.style.backgroundImage = `url('/data/backgrounds/bg-${bgId}.jpg')`;
};

class Options extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            opened: false
        };

        this.handleToggleOpened = this.handleToggleOpened.bind(this);
    }

    handleToggleOpened(event) {
        const { opened } = this.state;

        if (!opened) {
            document.addEventListener('click', this.handleToggleOpened)
        } else {
            if (hasParent(event.target, document.getElementById('options-popup'))) return;
            document.removeEventListener('click', this.handleToggleOpened)
        }

        this.setState({
            ...this.state,
            opened: !opened
        });
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleToggleOpened)
    }

    render() {
        const { updateSetting } = this.props;
        const { bgCount, shuffle, favorites, favorites_only } = this.props.settings;
        const { opened } = this.state;
        return (
            <div id="options">
                <a onClick={this.handleToggleOpened}>Options</a>
                {
                    opened && (
                        <div id="options-popup" style={{
                            zIndex: '999'
                        }}>
                            <ul className="list-simple list-inline">
                                {
                                    Array.apply(null, { length : bgCount }).map((dummy, index) => {
                                        const src = '/data/backgrounds/bg-' + (index + 1) + '.jpg';
                                        return (
                                            <li key={ index }>
                                                <div className={'circle'}>
                                                    <img className="img-responsive-h bg-preview" src={ src } onClick={ (event) => {
                                                        event.stopPropagation();
                                                        handleChangeBackground(index + 1)
                                                    } }/>
                                                </div>
                                                <button className="btn-corner star" onClick={
                                                    () => updateSetting(
                                                        'favorites',
                                                        'fs_favorites',
                                                        favorites.indexOf(index + 1) === -1 &&
                                                        [ ...favorites, index + 1].sort() ||
                                                        favorites.filter(f => f !== (index + 1))
                                                    )
                                                }>
                                                    {
                                                        favorites.indexOf(index + 1) === -1 &&
                                                        <span>&#x2606;</span> ||
                                                        <span>&#x2605;</span>
                                                    }
                                                </button>
                                            </li>
                                        );
                                    })
                                }
                            </ul>
                            <div>
                                <div className="inline checkbox-group">
                                    <input type="checkbox" id="shuffle"
                                           defaultValue={shuffle} defaultChecked={shuffle && "checked"}
                                           onChange={() => updateSetting(
                                               'shuffle',
                                               'fs_shuffle',
                                               !shuffle
                                           )}
                                    />
                                    <label htmlFor="shuffle">Shuffle</label>
                                </div>
                                <div className="inline checkbox-group">
                                    <input type="checkbox" id="favorites_only"
                                           defaultValue={favorites_only} defaultChecked={favorites_only && "checked"}
                                           onChange={() => updateSetting(
                                               'favorites_only',
                                               'fs_favorites_only',
                                               !favorites_only
                                           )}
                                    />
                                    <label htmlFor="favorites_only" onClick={() => {
                                        let warnBox = document.getElementById('favorites-warning');
                                        if (warnBox) {
                                            document.getElementById('favorites-warning').style.display = 'block';
                                        }
                                    }}>Favorites only</label>
                                </div>
                                {
                                    favorites.length === 0 && (
                                        <div id="favorites-warning" style={{color: 'red'}}>No favorites selected</div>
                                    )
                                }
                            </div>
                        </div>
                    )
                }
            </div>
        );
    }
}

export default Options;